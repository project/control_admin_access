<?php

namespace Drupal\control_admin_access;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Path\PathMatcher;
use Symfony\Component\HttpFoundation\IpUtils;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\HttpKernelInterface;

/**
 * Middleware for the Control Admin Access module.
 */
class CaaMiddleware implements HttpKernelInterface {

  /**
   * The decorated kernel.
   *
   * @var \Symfony\Component\HttpKernel\HttpKernelInterface
   */
  protected $httpKernel;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The patch matcher service.
   *
   * @var \Drupal\Core\Path\PathMatcher
   */
  protected $pathMatcher;

  /**
   * Constructs a BanMiddleware object.
   *
   * @param \Symfony\Component\HttpKernel\HttpKernelInterface $http_kernel
   *   The decorated kernel.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   * @param \Drupal\Core\Path\PathMatcher $path_matcher
   *   The patch matcher service.
   */
  public function __construct(HttpKernelInterface $http_kernel, ConfigFactoryInterface $config_factory, PathMatcher $path_matcher) {
    $this->httpKernel = $http_kernel;
    $this->configFactory = $config_factory;
    $this->pathMatcher = $path_matcher;
  }

  /**
   * {@inheritdoc}
   */
  public function handle(Request $request, int $type = self::MAIN_REQUEST, bool $catch = TRUE): Response {
    $this->request = $request;
    $config = $this->configFactory->get('control_admin_access.adminsettings');

    if (($this->blocked_urls = $config->get('blocked_urls')) && is_array($this->blocked_urls) && count($this->blocked_urls)) {
      $is_blocked_url = $this->checkUrl();

      if ($is_blocked_url && ($this->whitelist = $config->get('whitelist')) && is_array($this->whitelist) && count($this->whitelist)) {
        $in_whitelist = $this->checkIp();
        if (!$in_whitelist) {
          $response = new Response();
          $response->setStatusCode(401);
          return $response;
        }
      }
    }
    return $this->httpKernel->handle($request, $type, $catch);

  }

  /**
   * Check if current URL is inside the URLs bloqueds.
   */
  protected function checkUrl() {
    $patterns = implode("\n", $this->blocked_urls);
    return $this->pathMatcher->matchPath($this->request->getRequestUri(), $patterns);
  }

  /**
   * Check if client IP exists inside whitelist.
   */
  protected function checkIp() {
    $this->whitelist = array_filter(array_map('trim', $this->whitelist));
    return IpUtils::checkIp($this->request->getClientIp(), $this->whitelist);
  }

}
