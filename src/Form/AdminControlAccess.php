<?php

namespace Drupal\control_admin_access\Form;

/**
 * @file
 * Contains \Drupal\custom_checkout\Form\AdminCheckoutForm.
 */

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure example settings for this site.
 */
class AdminControlAccess extends ConfigFormBase {

  /**
   * Config variable to store configuration.
   *
   * @var string
   */
  private $checkoutAdminSettings = 'control_admin_access.adminsettings';

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      $this->checkoutAdminSettings,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'control_admin_access';
  }

  /**
   * {@inheritdoc}
   *
   * @FormElement("vertical_tabs");
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config($this->checkoutAdminSettings);
    $form['whitelist'] = [
      '#title' => $this->t('Enter IPs or IP ranges (Whitelist)'),
      '#type' => 'textarea',
      '#default_value' => is_array($config->get('whitelist')) ? implode("\n", $config->get('whitelist')) : '',
      '#description' => $this->t('Enter IP-address or IP range to allow access. Each entry in one line.'),
      '#attributes' => [
        'placeholder' => "127.0.0.1\n192.168.0.0/25",
      ],
    ];

    $form['blocked_urls'] = [
      '#title' => $this->t('Enter uRLs to block except for Whitelist'),
      '#type' => 'textarea',
      '#default_value' => is_array($config->get('blocked_urls')) ? implode("\n", $config->get('blocked_urls')) : '',
      '#description' => $this->t('Enter URLS to block. Each entry in one line.'),
      '#attributes' => [
        'placeholder' => "/*/admin\n/*/admin/*",
      ],
    ];

    $form['markup'] = [
      '#markup' => "<strong>WARNING</strong>: Adding information automatically the website will be blocker for all IPs out of range.\n<br>",
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config($this->checkoutAdminSettings)->set('whitelist', explode("\n", trim($form_state->getValue('whitelist'))))->save();
    $this->config($this->checkoutAdminSettings)->set('blocked_urls', explode("\n", trim($form_state->getValue('blocked_urls'))))->save();

    $this->messenger()->addMessage('All values have been saved.');
  }

}
