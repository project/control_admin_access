# Control admin access

This module allow block URLs in Drupal to avoid unexpected access.

This module can be usefull to allow access to admin part only for selected IPs.

## Requirements

This module requires no modules outside of Drupal core.

## Configuration

1. Configure Automated logout : Home >> Administration >> Configuration >> System >> Control access VPN

## Maintainers

- Oscar Embun - [programeta](https://www.drupal.org/u/programeta)
